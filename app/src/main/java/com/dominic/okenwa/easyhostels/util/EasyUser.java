package com.dominic.okenwa.easyhostels.util;

import android.content.SharedPreferences;

import java.util.Date;

/**
 * Created by live on 3/14/16.
 */
public class EasyUser {
    private int ID;
    private String fullname;
    private String email;
    private Date login_time;

    public EasyUser(int ID, String fullname, String email, Date login_time) {
        this.ID = ID;
        this.fullname = fullname;
        this.email = email;
        this.login_time = login_time;
    }

    public static EasyUser findUserByID(int userId) {
        return new EasyUser(userId,"Peace Okenwa", "pweetylicous@gmail.com", new Date());
    }

    public static EasyUser restoreUser(SharedPreferences pref){
        int ident = pref.getInt("current_user_id", -1);
        String name = pref.getString("current_user_name", "Default User");
        String email = pref.getString("current_user_email", "email@address.com");
        return new EasyUser(ident, name, email, new Date());
    }

    public Date getLogin_time() {
        return login_time;
    }

    public void setLogin_time(Date login_time) {
        this.login_time = login_time;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
