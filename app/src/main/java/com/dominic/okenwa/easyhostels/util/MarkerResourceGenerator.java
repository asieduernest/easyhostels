package com.dominic.okenwa.easyhostels.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.NinePatchDrawable;

import com.dominic.okenwa.easyhostels.R;

/**
 * Created by live on 3/14/16.
 */
public class MarkerResourceGenerator {
    private static final Bitmap.Config BITMAP_CONF;
    private static final String FONT_FACE = "Helvetica";
    private static final Typeface PRICE_TYPE_FACE;
    private static final Typeface RATING_TYPEFACE;

    static {
        PRICE_TYPE_FACE = Typeface.create(FONT_FACE, 1);
        RATING_TYPEFACE = Typeface.create(FONT_FACE, 0);
        BITMAP_CONF = Bitmap.Config.ARGB_8888;
    }

    public static Bitmap getMarker(Context context, String str, String str2, boolean z) {
        Resources resources = context.getResources();
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setTextAlign(Paint.Align.CENTER);
        Paint paint2 = new Paint(paint);
        paint2.setTextSize((float) context.getResources().getDimensionPixelSize(R.dimen.map_marker_price_font_size));
        paint2.setTypeface(PRICE_TYPE_FACE);
        Paint paint3 = new Paint(paint);
        paint3.setTextSize((float) context.getResources().getDimensionPixelSize(R.dimen.map_marker_rating_font_size));
        paint3.setTypeface(RATING_TYPEFACE);
        float[] fArr = new float[str.length()];
        paint2.getTextWidths(str, fArr);
        float f = 0.0f;
        for (float f2 : fArr) {
            f += f2;
        }
        NinePatchDrawable ninePatchDrawable = (NinePatchDrawable) resources.getDrawable(z ? R.drawable.ic_map_marker_highlighted : R.drawable.ic_map_marker);
        int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.map_marker_height);
        int max = Math.max((int) (f + (context.getResources().getDimension(R.dimen.map_marker_padding) * 2.0f)), resources.getDimensionPixelSize(R.dimen.map_marker_width));
        Bitmap createBitmap = Bitmap.createBitmap(max, dimensionPixelSize, BITMAP_CONF);
        Canvas canvas = new Canvas(createBitmap);
        ninePatchDrawable.setBounds(new Rect(0, 0, max, dimensionPixelSize));
        ninePatchDrawable.draw(canvas);
        canvas.drawText(str, (float) (canvas.getWidth() / 2), (float) resources.getDimensionPixelSize(R.dimen.map_marker_price_y), paint2);
        canvas.drawText("Gh¢", (float) (canvas.getWidth() / 2), (float) resources.getDimensionPixelSize(R.dimen.map_marker_rating_y), paint3);

        return createBitmap;
    }
}
