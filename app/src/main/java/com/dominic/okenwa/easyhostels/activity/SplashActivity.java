package com.dominic.okenwa.easyhostels.activity;

import android.content.Intent;
import android.os.Bundle;

import com.daimajia.androidanimations.library.Techniques;
import com.dominic.okenwa.easyhostels.MainActivity;
import com.dominic.okenwa.easyhostels.R;
import com.dominic.okenwa.easyhostels.intro.QuickIntro;
import com.dominic.okenwa.easyhostels.util.EasyUtil;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

public class SplashActivity extends AwesomeSplash {

    @Override
    public void initSplash(ConfigSplash configSplash) {

            /* you don't have to override every property */

        //Customize Circular Reveal
        configSplash.setBackgroundColor(R.color.strokeColor); //any color you want form colors.xml
        configSplash.setAnimCircularRevealDuration(1000); //int ms
        configSplash.setRevealFlagX(Flags.REVEAL_RIGHT);  //or Flags.REVEAL_LEFT
        configSplash.setRevealFlagY(Flags.REVEAL_BOTTOM); //or Flags.REVEAL_TOP

        //Choose LOGO OR PATH; if you don't provide String value for path it's logo by default

        //Customize Logo
        configSplash.setLogoSplash(R.drawable.splash_icon); //or any other drawable
        configSplash.setAnimLogoSplashDuration(1500); //int ms
        configSplash.setAnimLogoSplashTechnique(Techniques.FadeIn); //choose one form Techniques (ref: https://github.com/daimajia/AndroidViewAnimations)


        //Customize Title
        configSplash.setTitleSplash("Easy Hostels™");
        configSplash.setTitleTextColor(R.color.secondary_text);
        configSplash.setTitleTextSize(20f); //float value
        configSplash.setAnimTitleDuration(1700);
        configSplash.setAnimTitleTechnique(Techniques.FadeIn);

    }
    public void tests(){
        try {
            ourOwnBadFunction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void ourOwnBadFunction() throws Exception {
        boolean somethngBad = true;
        if(somethngBad){
            throw new Exception();
        }
    }
    @Override
    public void animationsFinished(){


        Bundle data = new Bundle();
        if(!EasyUtil.checkFirstTime(this)){
            Class startClass = null;
            if(EasyUtil.checkLoggedIn(this)){
                startClass = MainActivity.class;
                data.putString("mode", "fromPhone");
            }
            else {
                startClass = LoginActivity.class;
                data.putString("mode", "fromLogin");
            }
            startActivity(new Intent(SplashActivity.this, startClass).putExtras(data));
            this.finish();
        }
        else {
            // Show intro
            startActivity(new Intent(this, QuickIntro.class));
            this.finish(); // dun wanna come back!
        }


    }
}
