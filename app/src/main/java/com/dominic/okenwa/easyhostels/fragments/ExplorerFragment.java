package com.dominic.okenwa.easyhostels.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.dominic.okenwa.easyhostels.R;
import com.dominic.okenwa.easyhostels.activity.HostelDetailView;
import com.dominic.okenwa.easyhostels.graphics.CustomDimEffect;
import com.dominic.okenwa.easyhostels.helpers.RestHelper;
import com.dominic.okenwa.easyhostels.helpers.RestStatic;
import com.dominic.okenwa.easyhostels.models.MarkerData;
import com.dominic.okenwa.easyhostels.util.EasyUtil;
import com.dominic.okenwa.easyhostels.util.MarkerResourceGenerator;
import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mingle.entity.MenuEntity;
import com.mingle.sweetpick.RecyclerViewDelegate;
import com.mingle.sweetpick.SweetSheet;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ExplorerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ExplorerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExplorerFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnCameraChangeListener, FloatingSearchView.OnMenuItemClickListener, FloatingSearchView.OnLeftMenuClickListener, GoogleMap.OnMarkerClickListener, SweetSheet.OnMenuItemClickListener, GoogleMap.OnInfoWindowClickListener {

    public static ArrayList<Marker> VisualMarkers;
    public static Location currentPosition;
    public static View explorerView;
    public static Marker me;
    public static CameraPosition savedPos;
    public static boolean showing_sweet;
    public static SweetSheet sweetme;
    private float LOCATION_REFRESH_DISTANCE;
    private long LOCATION_REFRESH_TIME;
    private final int REQ_CODE_SPEECH_INPUT = 0;
    public Context context;
    public DrawerLayout drawer;
    public FrameLayout fl;
    public FrameLayout flm;
    public FloatingSearchView flsv;
    public FragmentManager frag_man;
    public OnFragmentInteractionListener mListener;
    private LocationManager mLocationManager;
    public GoogleMap map;
    public SupportMapFragment mapFragment;
    private Circle rad;
    public RelativeLayout rl;
    private boolean showing;


    public ExplorerFragment() {
        // Required empty public constructor
    }

    public static ExplorerFragment newInstance(DrawerLayout drawerLayout, Context c) {
        ExplorerFragment fragment = new ExplorerFragment();
        fragment.drawer = drawerLayout;
        fragment.context = c;
        return fragment;
    }

    public static ExplorerFragment newInstance() {
        ExplorerFragment ex = new ExplorerFragment();
        return ex;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Activity mact = this.getActivity();
        mLocationManager = (LocationManager) mact.getSystemService(mact.LOCATION_SERVICE);
        LocationListener mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                ExplorerFragment.currentPosition = location;
                ExplorerFragment explorerFragment = ExplorerFragment.this;
                if (ExplorerFragment.me == null || ExplorerFragment.this.rad == null) {
                    ExplorerFragment.this.rad = ExplorerFragment.this.map.addCircle(new CircleOptions().center(new LatLng(location.getLatitude(), location.getLongitude())).radius(Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(ExplorerFragment.this.context).getString("pref_search_radius", "1")) * 1000.0d).fillColor(R.color.colorPrimaryLight));
                    ExplorerFragment.me = ExplorerFragment.this.map.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("You are here").icon(BitmapDescriptorFactory.fromResource(R.drawable.mark)).flat(false));
                    return;
                }
                ExplorerFragment.me.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
                ExplorerFragment.this.rad.setCenter(new LatLng(location.getLatitude(), location.getLongitude()));
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {
                Snackbar.make(ExplorerFragment.this.getView(), "GPS Device Turned ON", Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        };

        if (ActivityCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME,
                LOCATION_REFRESH_DISTANCE, mLocationListener);

    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(this.getActivity().getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View explorer_fragment_view;

        explorer_fragment_view = inflater.inflate(R.layout.fragment_explorer, container, false);
        Fragment fragment = (Fragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment = (SupportMapFragment) fragment;
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        setupBottomSheet(explorer_fragment_view);
        setupFloatingSearch(explorer_fragment_view);
        final FloatingActionButton fb = (FloatingActionButton) explorer_fragment_view.findViewById(R.id.btnFilter);
        final FloatingActionButton fb_refresh = (FloatingActionButton) explorer_fragment_view.findViewById(R.id.btnRefresh);
        fb_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fb.setIndeterminate(true);
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ExplorerFragment.this.context);
                String set = prefs.getString("pref_search_mode", "2");
                if (set.equals("1")) {
                    String radset = prefs.getString("pref_search_radius", "1");
                    if (ExplorerFragment.me != null) {
                        RestHelper.getHostelsByRadius(radset, ExplorerFragment.me.getPosition(), fb, ExplorerFragment.this.mapFragment, ExplorerFragment.this.getActivity());
                        return;
                    }
                    Snackbar.make(ExplorerFragment.this.getActivity().getCurrentFocus(), (CharSequence) "Cannot Get Current Position", Snackbar.LENGTH_LONG).show();
                    fb.setIndeterminate(false);
                } else if (set.equals("2")) {
                    RestHelper.getHostelsByBounds(RestStatic.lookingAt, fb, ExplorerFragment.this.mapFragment, ExplorerFragment.this.getActivity());
                }
            }
        });

        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog md = new MaterialDialog.Builder(ExplorerFragment.this.getActivity())
                        .title("Filter Resuts")
                        .customView(R.layout.dialog_filter, true)
                        .positiveText("Search")
                        .show();

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ExplorerFragment.this.context);
                String set = prefs.getString("pref_search_mode", "2");
                String min;
                String max;


            }
        });

        mapFragment.getMap().setOnCameraChangeListener(this);

        if (ExplorerFragment.savedPos == null) {

        }

        this.explorerView = explorer_fragment_view;
        return this.explorerView;

    }

    private void setupFloatingSearch(View explorer_fragment_view) {
        flsv = (FloatingSearchView) explorer_fragment_view.findViewById(R.id.floating_search_view);
        flsv.setOnLeftMenuClickListener(this);
        flsv.setOnMenuItemClickListener(this);
    }

    private void setupBottomSheet(View explorer_fragment_view) {

        final ArrayList<MenuEntity> list = new ArrayList<>();
        fl = (FrameLayout)explorer_fragment_view.findViewById(R.id.explore_frame);

        MenuEntity menuViewHostel = new MenuEntity();
        menuViewHostel.iconId = R.drawable.ic_remove_red_eye_black_24dp;
        menuViewHostel.title = "Show Details";

        MenuEntity menuDirectionsHostel = new MenuEntity();
        menuDirectionsHostel.iconId = R.drawable.ic_directions_black_24dp;
        menuDirectionsHostel.title = "Get Directions";

        MenuEntity menuFavoriteHostel = new MenuEntity();
        menuFavoriteHostel.iconId = R.drawable.ic_favorite_black_24dp;
        menuFavoriteHostel.title = "Add to favorites";

        list.add(menuViewHostel);
        list.add(menuDirectionsHostel);
        list.add(menuFavoriteHostel);

        sweetme = new SweetSheet(fl);
        sweetme.setMenuList(list);
        sweetme.setDelegate(new RecyclerViewDelegate(false));
        sweetme.setBackgroundEffect(new CustomDimEffect(0.65f));
        sweetme.setOnMenuItemClickListener(this);

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        map.getUiSettings().setMapToolbarEnabled(false);

        LatLng accra = new LatLng(5.603662, 0.163625);
        CameraPosition cam = new CameraPosition(accra, 10, 10, 0);
        if (ExplorerFragment.savedPos != null) {
            map.moveCamera(CameraUpdateFactory.newCameraPosition(ExplorerFragment.savedPos));
            if(!RestStatic.hostels_str.isEmpty()){
                try {
                    RestHelper.createMarkersFromJson(RestStatic.hostels_str, googleMap, this.getActivity());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            if(currentPosition != null){
                LatLng curPo = new LatLng(currentPosition.getLatitude(), currentPosition.getLongitude());
                CameraPosition camPo = new CameraPosition(curPo, 7, 10, 0);
                map.animateCamera(CameraUpdateFactory.newCameraPosition(camPo), 3000, null);
            }
            else {
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cam), 5000, null);
            }
        }

        map.setOnMarkerClickListener(this);
        map.setOnInfoWindowClickListener(this);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == getActivity().RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String str = result.get(0);
                    EasyUtil.vibrate(this.getContext(), 500); // Shake!
                    flsv.setSearchFocused(true);
                    flsv.setSearchText(str);
                }
                break;
            }

        }
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        ExplorerFragment.savedPos = cameraPosition;
    }

    @Override
    public void onActionMenuItemSelected(MenuItem item) {

        int ident = item.getItemId();
        switch (ident) {
            case R.id.action_location:

                if(currentPosition != null){
                    Snackbar.make(this.getView(), "Getting Current Coordinates", Snackbar.LENGTH_SHORT).show();
                    LatLng curPo = new LatLng(currentPosition.getLatitude(), currentPosition.getLongitude());
                    CameraPosition camPo = new CameraPosition(curPo, 18, 45, 0);
                    map.animateCamera(CameraUpdateFactory.newCameraPosition(camPo), 8000, null);
                }
                else {
                    Snackbar.make(this.getView(), "We could not locate a GPS Satellite ", Snackbar.LENGTH_LONG).show();
                }
                break;
            case R.id.action_voice_rec:
                promptSpeechInput();
                break;
            default:
                break;
        }
    }

    @Override
    public void onMenuOpened() {
        this.drawer.openDrawer(GravityCompat.START);
    }

    @Override
    public void onMenuClosed() {
        this.drawer.closeDrawer(GravityCompat.START);
    }

    public void restoreSearchView() {
        this.flsv.closeMenu(true);
    }

    public void openSearchView() {
        this.flsv.openMenu(true);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (!marker.equals(me)) {
            chooseMarker(marker);
        }
        return false;
    }

    private void chooseMarker(Marker marker) {
        refreshMarkers();
        MarkerData md = (MarkerData) RestStatic.map.get(marker);
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(MarkerResourceGenerator.getMarker(this.context, String.valueOf(md.getDisplay_price()), String.valueOf(md.getRating()), true)));
    }

    @Override
    public boolean onItemClick(int position, MenuEntity menuEntity) {
        if(position == 0){
            Toast.makeText(ExplorerFragment.this.getActivity(), "Showing Detail Page", Toast.LENGTH_LONG).show();
        }
        else if(position == 1){
            Toast.makeText(ExplorerFragment.this.getActivity(), "Getting Directions", Toast.LENGTH_LONG).show();
        }
        else if(position == 2){
            Toast.makeText(ExplorerFragment.this.getActivity(), "Adding to Favorites", Toast.LENGTH_LONG).show();
        }

        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if(RestStatic.hostels_str != "") {
            RestStatic.current_hostel = (MarkerData) RestStatic.map.get(marker);
            startActivity(new Intent(getContext(), HostelDetailView.class));
        }
    }

    private void refreshMarkers() {
        if (RestStatic.markers != null && RestStatic.map != null) {
            Iterator it = RestStatic.markers.iterator();
            while (it.hasNext()) {
                Marker r = (Marker) it.next();
                MarkerData md = (MarkerData) RestStatic.map.get(r);
                r.setIcon(BitmapDescriptorFactory.fromBitmap(MarkerResourceGenerator.getMarker(this.context, String.valueOf(md.getDisplay_price()), String.valueOf(md.getRating()), false)));
            }
        }
    }



    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }
}