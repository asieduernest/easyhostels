package com.dominic.okenwa.easyhostels.util;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by live on 3/8/16.
 */
public class EasyConfig {
    public static final String DONATE_URL = "http://google.com.gh";
    public static final String REST_URL = "http://vvuconnect.net/e-service/";
    public static final LatLng CURR = null;
    public static final String TAG = "EasyHostels";
    public static final String PREFS_NAME = "EasyPrefs";
}
