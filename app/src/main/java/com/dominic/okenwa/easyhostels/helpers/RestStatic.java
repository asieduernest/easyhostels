package com.dominic.okenwa.easyhostels.helpers;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;

import com.dominic.okenwa.easyhostels.activity.HostelDetailView;
import com.dominic.okenwa.easyhostels.models.MarkerData;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by live on 5/8/16.
 */
public class RestStatic {
    public static String hostels_str = "";
    public static HashMap<Marker, MarkerData> map;
    public static ArrayList<Marker> markers;
    public static MarkerData current_hostel;
    public static LatLngBounds lookingAt;

    public void showEmailIntent(String to, String title, String msg) {

    }

    public static void sendEmail(HostelDetailView hostelDetailView, String contact_email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", contact_email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Enquiries");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        hostelDetailView.startActivity(Intent.createChooser(emailIntent, "Send Owner Email"));
    }

    public static void callNumber(HostelDetailView hostelDetailView, String contact_phone) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contact_phone));
        if (ActivityCompat.checkSelfPermission(hostelDetailView, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        hostelDetailView.startActivity(intent);
    }

    public static void getDirections(HostelDetailView hostelDetailView, Double lat, Double lng) {
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lat, lng);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        hostelDetailView.startActivity(intent);
    }
}
